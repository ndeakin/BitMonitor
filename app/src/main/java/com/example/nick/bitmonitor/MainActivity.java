package com.example.nick.bitmonitor;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private static final String BASE_URL = "https://min-api.cryptocompare.com";
    private static final String PRICE_EXT_FMT = "/data/price?fsym=%s&tsyms=%s";

    private static final String CRYPTOCURRENCY_BTC = "BTC";
    private static final String CRYPTOCURRENCY_ETH = "ETH";
    private static final String CRYPTOCURRENCY_ZEN = "ZEN";
    private static final String CRYPTOCURRENCY_EOS = "EOS";

    private static final String CURRENCY_USD = "USD";
    private static final String CURRENCY_CAD = "CAD";

    private void MakeRequest() {
        Log.d("test","MakeRequest() called");

        RequestQueue queue = Volley.newRequestQueue(this);
        String requestUrl = String.format(BASE_URL + PRICE_EXT_FMT, CRYPTOCURRENCY_BTC, CURRENCY_USD);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("test","onResponse() called");
                        Fragment fragment = getSupportFragmentManager().getFragments().get(0);
                        if(fragment instanceof CryptocurrencyListFragment) {
                            Log.d("test","onResponse() - is fragment placeholder");
                            CryptocurrencyListFragment CryptocurrencyListFragment = (CryptocurrencyListFragment)fragment;

                            // Parse JSON
                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                String price = jsonObject.getString(CURRENCY_USD);
                                CryptocurrencyListFragment.setmTextViewText("Price in USD is " + price);
                            } catch( JSONException e ) {
                                CryptocurrencyListFragment.setmTextViewText("Failed to parse JSON response: " + e.toString());
                                return;
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("test","onErrorResponse() called");
                Fragment fragment = getSupportFragmentManager().getFragments().get(0);
                if(fragment instanceof CryptocurrencyListFragment) {
                    Log.d("test","onErrorResponse() - is fragment placeholder");
                    CryptocurrencyListFragment CryptocurrencyListFragment = (CryptocurrencyListFragment) fragment;
                    CryptocurrencyListFragment.setmTextViewText("That didn't work: " + error.toString());
                }
            }
        });

        queue.add(stringRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Making request...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                MakeRequest();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            String msg = String.format("getItem for position %d", position);
            Log.d("Test", msg);
            // getItem is called to instantiate the fragment for the given page.
            // Return a CryptocurrencyListFragment (defined as a static inner class below).
            switch(position) {
                case 0:
                    return CryptocurrencyListFragment.newInstance(position + 1);
                case 1:
                    return AddressListFragment.newInstance(position + 1);
            }
            return new Fragment();
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }
}
